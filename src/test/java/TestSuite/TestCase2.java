package TestSuite;

import org.testng.annotations.Test;

public class TestCase2 {
    @Test (groups={"SmokeTC"})
    public void display1()
    {
        System.out.println("Second Testcase called");
    }

    @Test (enabled=false)
    public void display3()
    {
        System.out.println("Third Testcase called");
    }

    @Test
    public void testy()
    {
        System.out.println("Not-display method names in TestCase2 class");
    }
}
