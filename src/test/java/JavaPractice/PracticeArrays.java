package JavaPractice;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PracticeArrays {

    public static void main (String []args)
    {
        //practice ArrayList of Strings
        ArrayList<String> Restaurant = new ArrayList<>();
        Restaurant.add("Juice");
        Restaurant.add("Sandwich");
        Restaurant.add("Coffee");
        Restaurant.add("Bisleri Water bottle");
        Restaurant.add("Coffee");
        System.out.println(Restaurant);
        System.out.println();
        Restaurant.add(3, "Pizza");
        System.out.println(Restaurant);
        Restaurant.remove(5);
        System.out.println(Restaurant);
        System.out.println();
        System.out.println("1st element of this arraylist is " + Restaurant.get(0));

        System.out.println(Restaurant.hashCode());
        System.out.println();
        Restaurant.set(3, "Hamburger");
        System.out.println(Restaurant);

        Collections.sort(Restaurant);
        System.out.println("Sorted Restaurant menu is " + Restaurant);


        int d= Restaurant.size();
        System.out.println();
        System.out.println("size of this Array list is " + d +" elements");

        //practice ArrayList of Integers === Not possible ?? Ask Vishal how to create list of integers ?? Just using
        // simple array concepts?
        ArrayList <Integer> IntList = new ArrayList<Integer>();
        IntList.add(21);
        IntList.add(26);
        IntList.add(23);
        System.out.println("Arraylist with integer values is here: " + IntList);

        Collections.sort(IntList);
        System.out.println();
        System.out.println("sorted ArrayList of Integers is: " + IntList);

        //List<int> Prices = new ArrayList<>();

        System.out.println();
        int a[]={33,3,4,5};//declaration, instantiation and initialization
        //printing array
        for(int i=0;i<a.length;i++)//length is the property of array
        {
            System.out.println(a[i]);
    }
        Restaurant.clear();
        System.out.println();
        System.out.println("Empty list using clear function and then see output");
        System.out.println(Restaurant);
        System.out.println();
        System.out.println("Checking now if list is empty after clear method called");
        System.out.println(Restaurant.isEmpty());
    }
}

// Find second smallest number of 10 integer elements in an array == either using swap concept OR ArrayList

// example of set, hashmap, arraylist, list, stringArray object creation = total 4 also practice
