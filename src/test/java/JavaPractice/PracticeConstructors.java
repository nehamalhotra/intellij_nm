package JavaPractice;

public class PracticeConstructors {
    int id;
    // ask vishal ??????????????????? on page = https://www.javatpoint.com/final-keyword
    //final int id;
    String name;
    int salary;
    String department;
    static String company = "Infosys";
    //final String company = "Infosys";




    //this is an example of a parameterized constructor
    PracticeConstructors(int i, String n, int sal, String dept)
    {
       // company="TCS";
      //  changeMethod();
        id=i;
        name=n;
        salary=sal;
        department = dept;

    }
    static void changeMethod() {
       company = "BBDIT";
   }

    void displayEmployeeInfo ()
    {
               System.out.println("Company = " + company + " Employee ID:"+ id + " Name: " + name + " " + " Salary: " + salary + " & " +
                "Department: " + department);
    }

    static
    {
        company="HP";
        System.out.println("Hi Static block is executed");
    }

    public static void main (String []args)
    {
        PracticeConstructors emp1 = new PracticeConstructors(1234,"Neha", 750000, "HR");
        PracticeConstructors emp2 = new PracticeConstructors(4567,"Raj", 450000, "Admin");
        PracticeConstructors emp3 = new PracticeConstructors(7891,"Ron", 650000, "Sales");

        emp1.displayEmployeeInfo();
        emp2.displayEmployeeInfo();
        emp3.displayEmployeeInfo();

    }
}
