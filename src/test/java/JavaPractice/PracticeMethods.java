package JavaPractice;

public class PracticeMethods {

    public static void main (String []args)
    {
        //use Practice1 class now and create its object - automatically default constructor in that class will be called first each time at object creation
        Practice1 emp1 = new Practice1();

        // initializing values of object using method EmployeeDepartmentInformation
        emp1.EmployeeDepartmentInformation(1234,"Neha", 750000,"HR");

        Practice1 emp2 = new Practice1();
        emp2.EmployeeDepartmentInformation(1235,"Rohit", 470000, "Admin");

        Practice1 emp3 = new Practice1();
        emp3.EmployeeDepartmentInformation(1236,"Sonam", 420000, "IT helpdesk");

        Practice1 emp4 = new Practice1();
        emp4.EmployeeDepartmentInformation(1255,"Sonam", 805735, "HR");

    }

}
