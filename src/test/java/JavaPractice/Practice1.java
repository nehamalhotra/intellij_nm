package JavaPractice;

import java.sql.SQLOutput;

public class Practice1 {
Practice1()
    {
        //this is an example of default constructor ?
        System.out.println("Reading Employee information");
    }

    int id;
    String name;
    float salary;
    String department;
    boolean departmentHR;

    public void EmployeeDepartmentInformation (int i, String n, float sal, String dept)
    {
        id=i;
        name=n;
        salary=sal;
        department = dept;

        if (department == "HR")
        {
            departmentHR = true;
            System.out.println("Employee ID:"+ id + " Name: " + name + " " + "is in the HR department" + " & " + "the" +
                    " Salary " + "is " + sal + " & boolean value is = " + departmentHR);
        }
        else
        {
            departmentHR = false;
            System.out.println("Employee ID:"+ id + " Name: " + name + " " + "is NOT from the HR department" + " & " + "the Salary " + "is " + sal + " & boolean value is = " + departmentHR);
        }

    }
}
