import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.time.Duration;

public class GmailExample {
    public static void main(String [] Array1)
    {
        // this is a key and value setting for chrome driver (to let jvm know - where to find chrome driver extension)
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\ZX13\\IdeaProjects\\Tester\\lib\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.google.com/intl/en-GB/gmail/about/#");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.findElement(By.xpath("//a[contains(text(),'Sign in')]/../a[3]")).click();

        driver.findElement(By.name("firstName")).sendKeys("Neha");
        driver.findElement(By.name("lastName")).sendKeys("Malhotra");
        driver.findElement(By.id("username")).sendKeys("nehamalhotra11.8878");
        driver.findElement(By.name("Passwd")).sendKeys("abcd@123456");
        driver.findElement(By.name("ConfirmPasswd")).sendKeys("abcd@123456");
        driver.findElement(By.xpath("//span[contains(text(),'Next')]")).click();
        //driver.findElement(By.xpath("//span[contains(text(),'Next')]")).sendKeys(Keys.ENTER);

    }
    @Test
    public void display1()
    {
       System.out.println("Hi, This is annotations example");
    }

    @Test
    public void display2()
    {
        System.out.println("Hi, This is annotations example via testng.xml");
    }
}
