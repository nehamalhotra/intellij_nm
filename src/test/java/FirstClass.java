import java.util.ArrayList;

public class FirstClass {
    public static void main(String[] array1)
    {
        System.out.println("This is First Array demo");
        ArrayList<String> list=new ArrayList<String>();//Creating arraylist
        list.add("Mango");//Adding object in arraylist
        list.add("Apple");
        list.add("Banana");
        list.add("Grapes");
        //Printing the arraylist object
        System.out.println(list);
        list.clear();
        System.out.println(list);
    }
}
